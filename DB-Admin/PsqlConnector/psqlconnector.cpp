#include "psqlconnector.hpp"

PsqlConnector::PsqlConnector(QString a_Host, int a_Port, QString a_User, QString a_Pass, QString a_Database)
{
	qDebug() << QSqlDatabase::drivers();

	if (QSqlDatabase::drivers().contains("QPSQL"))
	{
		Database = QSqlDatabase::addDatabase("QPSQL");
		Database.setHostName(a_Host);
		Database.setUserName(a_User);
		Database.setPassword(a_Pass);
		//Database.setDatabaseName(a_Database);

		if (!Database.open())
		{
			qDebug() << Database.lastError().text();
		}
	}
	else
	{
		qDebug() << "No PSQL Driver!";
	}
}

PsqlConnector::PsqlConnector(const PsqlConnector & a_Other)
{
	Database = a_Other.Database;
}

PsqlConnector::~PsqlConnector()
{

}

QList<QMap<QString, QVariant>> PsqlConnector::Query(QString a_Query)
{
	QList<QMap<QString, QVariant>> Res;

	QSqlQuery query(Database);
	if (query.exec(a_Query))
	{
		while (query.next())
		{
			QMap<QString, QVariant> Row;
			QSqlRecord r = query.record();
			int fields = r.count();

			for (int i = 0; i < fields; ++i)
			{
				QSqlField f = r.field(i);
				Row.insert(f.name(), f.value());
			}
			Res.append(Row);
		}
	}
	else
	{
		qDebug() << query.lastError();
	}
	return Res;
}

void PsqlConnector::SetDB(QString a_DB)
{
	Database.close();
	Database.setDatabaseName(a_DB);
	Database.open();
}
