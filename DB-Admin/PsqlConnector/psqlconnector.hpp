#ifndef PSQLCONNECTOR_HPP
#define PSQLCONNECTOR_HPP

#include "psqlconnector_global.h"
#include <QtSql>

class PSQLCONNECTOR_EXPORT PsqlConnector
{
public:
	PsqlConnector(QString a_Host, int a_Port, QString a_User, QString a_Pass, QString a_Database);
	PsqlConnector(const PsqlConnector& a_Other);
	~PsqlConnector();
	
	QList<QMap<QString, QVariant>> Query(QString a_Query);
	void SetDB(QString a_DB);

private:
	QSqlDatabase Database;

};

#endif // PSQLCONNECTOR_HPP
