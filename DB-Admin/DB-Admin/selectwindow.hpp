#pragma once

#include <QWidget>
#include "ui_selectwindow.h"

class SelectWindow : public QWidget
{
	Q_OBJECT

public:
	SelectWindow(class SqlConnector* a_Connection, QString a_Table, QString a_Filter, QString a_Sort, QWidget *parent = Q_NULLPTR);
	~SelectWindow();

private:
	Ui::SelectWindow ui;
	class SqlConnector* Connection;
	class QSqlTableModel* ResultsModel;

private slots:
	void RunQuery();
};
