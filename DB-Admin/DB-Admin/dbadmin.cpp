#include "dbadmin.hpp"
#include "connectioncontainer.hpp"
#include "newconnection.hpp"
#include "psql.hpp"


DBAdmin::DBAdmin(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	connect(ui.mnuNewConnection, SIGNAL(triggered()), this, SLOT(AddConnection()));
}

DBAdmin::~DBAdmin()
{

}


void DBAdmin::AddConnection()
{
	auto NewConnect = new NewConnection();

	if (NewConnect->exec() == QDialog::DialogCode::Accepted)
	{
		for (int i = 0; i < ui.tabConnections->count(); ++i)
		{
			if (ui.tabConnections->tabText(i) == NewConnect->Host)
			{
				return;
			}
		}
		auto Connection = new PSQL(NewConnect->Host, NewConnect->Port, NewConnect->User, NewConnect->Pass);
		ui.tabConnections->addTab(new ConnectionContainer(Connection), NewConnect->Host);
	}

	

}