#pragma once

#include <QDialog>
#include "ui_newconnection.h"

class NewConnection : public QDialog
{
	Q_OBJECT

public:
	NewConnection(QWidget *parent = Q_NULLPTR);
	~NewConnection();

	QString Host;
	QString User;
	QString Pass;
	int Port;
	QString DB;

private:
	Ui::NewConnection ui;

private slots:
	void Connect();
};
