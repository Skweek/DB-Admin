#pragma once
#include "sqlconnector.hpp"


class PSQL : public SqlConnector
{
public:
	PSQL(QString a_Host, int a_Port, QString a_User, QString a_Pass);
	~PSQL();

	QList<QMap<QString, QVariant>> Query(QString a_Query);

	void SetDB(QString a_DB);
};

