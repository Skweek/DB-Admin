#ifndef SQLCONNECTOR_HPP
#define SQLCONNECTOR_HPP

#include <QtSql>


class SqlConnector
{
public:
	static const QString GET_TABLES;
	static const QString GET_VIEWS;
	static const QString GET_FUNCTIONS;
	static const QString GET_SEQUENCES;
	static const QString GET_TYPES;
public:
	SqlConnector() {}
	virtual ~SqlConnector() {}

	virtual QList<QMap<QString, QVariant>> Query(QString a_Query) = 0;
	virtual void SetDB(QString a_DB) = 0;
	QSqlDatabase GetConnection() { return Database; }

protected:
	QSqlDatabase Database;


};

#endif // SQLCONNECTOR_HPP
