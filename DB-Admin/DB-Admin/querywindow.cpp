#include "querywindow.hpp"
#include "connectioncontainer.hpp"
#include "psql.hpp"

QueryWindow::QueryWindow(class SqlConnector* a_Connection, QString a_DB, QString a_Query, QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);
	Connection = a_Connection;
	Connection->SetDB(a_DB);

	if (!a_Query.isEmpty())
	{
		ui.txtQuery->setText(a_Query);
		RunQuery();
	}

	connect(ui.btnRun, SIGNAL(clicked()), this, SLOT(RunQuery()));
}

QueryWindow::~QueryWindow()
{
}

void QueryWindow::RunQuery()
{

	QApplication::setOverrideCursor(Qt::WaitCursor);


	ui.tblResults->setRowCount(0);
	ui.tblResults->setColumnCount(0);
	
	QTime myTimer;
	myTimer.start();	
	auto Res = Connection->Query(ui.txtQuery->toPlainText());

	QStringList Headers;
	for (int f = 0; f < Res.at(0).keys().count(); ++f)
	{
		Headers.append(Res.at(0).keys().at(f));
		ui.tblResults->insertColumn(f);
	}
	ui.tblResults->setHorizontalHeaderLabels(Headers);
	
	int Row = 0;
	for each (auto var in Res)
	{
		ui.tblResults->insertRow(Row);
		int Column = 0;
		for each (auto field in var.values())
		{
			ui.tblResults->setItem(Row, Column, new QTableWidgetItem(field.toString()));
			++Column;
		}

		++Row;
	}


	QApplication::restoreOverrideCursor();
}