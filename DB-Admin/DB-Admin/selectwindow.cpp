#include "selectwindow.hpp"
#include "connectioncontainer.hpp"
#include "psql.hpp"
#include <QSqlTableModel>

SelectWindow::SelectWindow(class SqlConnector* a_Connection, QString a_Table, QString a_Filter, QString a_Sort, QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);


	ResultsModel = new QSqlTableModel(this, a_Connection->GetConnection());
	ResultsModel->setTable(a_Table);
	ui.txtFilter->setText(a_Filter);
	ResultsModel->setSort(0, Qt::AscendingOrder);
	ResultsModel->setEditStrategy(QSqlTableModel::OnFieldChange);

	RunQuery();

	ui.tblResults->setModel(ResultsModel);


	connect(ui.btnRun, SIGNAL(clicked()), this, SLOT(RunQuery()));
}

SelectWindow::~SelectWindow()
{

}

void SelectWindow::RunQuery()
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	QTime myTimer;
	myTimer.start();

	ResultsModel->setFilter(ui.txtFilter->toPlainText());
	ResultsModel->select();

	QApplication::restoreOverrideCursor();
	int nMilliseconds = myTimer.elapsed();
	qDebug() << nMilliseconds;


	auto Res = ResultsModel->lastError();
	if (Res.type() != QSqlError::NoError)
	{
		qDebug() << Res;
	}
}