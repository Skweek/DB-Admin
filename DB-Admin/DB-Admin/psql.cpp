#pragma once
#include "psql.hpp"

//

const QString SqlConnector::GET_TABLES = "SELECT table_name as name FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'BASE TABLE' ORDER BY name ASC; ";
const QString SqlConnector::GET_VIEWS = "SELECT table_name as name FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'VIEW' ORDER BY name ASC; ";
const QString SqlConnector::GET_FUNCTIONS = "SELECT p.proname || ' (' || oidvectortypes(p.proargtypes) || ')' AS name FROM pg_proc p INNER JOIN pg_namespace ns ON (p.pronamespace = ns.oid) where ns.nspname = 'public' ORDER BY name ASC; ";
const QString SqlConnector::GET_SEQUENCES = "SELECT c.relname AS name FROM pg_class c WHERE c.relkind = 'S'; ";
const QString SqlConnector::GET_TYPES = "SELECT t.typname as NAME FROM pg_type t LEFT JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace \
										WHERE(t.typrelid = 0 OR(SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid)) \
										AND NOT EXISTS(SELECT 1 FROM pg_catalog.pg_type el WHERE el.oid = t.typelem AND el.typarray = t.oid) \
										AND n.nspname NOT IN('pg_catalog', 'information_schema');";

PSQL::PSQL(QString a_Host, int a_Port, QString a_User, QString a_Pass)
{
	qDebug() << QSqlDatabase::drivers();

	if (QSqlDatabase::drivers().contains("QPSQL"))
	{
		Database = QSqlDatabase::addDatabase("QPSQL", a_Host);
		Database.setHostName(a_Host);
		Database.setUserName(a_User);
		Database.setPassword(a_Pass);

		if (!Database.open())
		{
			qDebug() << Database.lastError().text();
		}
	}
	else
	{
		qDebug() << "No PSQL Driver!";
	}
}

PSQL::~PSQL() {}

QList<QMap<QString, QVariant>> PSQL::Query(QString a_Query)
{
	QList<QMap<QString, QVariant>> Res;

	QTime myTimer;
	myTimer.start();

	QSqlQuery query(Database);

	query.setForwardOnly(true); // We move the data elsewhere so we don't need to cache the results
	if (query.exec(a_Query))
	{
		int queryTime = myTimer.elapsed();
		while (query.next())
		{
			QMap<QString, QVariant> Row;
			QSqlRecord r = query.record();
			int fields = r.count();

			for (int i = 0; i < fields; ++i)
			{
				QSqlField f = r.field(i);
				Row.insert(f.name(), f.value());
			}
			Res.append(Row);
		}
		int totalTime = myTimer.elapsed();

		qDebug() << "Query Took: " + QString::number(queryTime) + "ms";
		qDebug() << "Results Took: " + QString::number(totalTime) + "ms";
	}
	else
	{
		qDebug() << query.lastError();
	}
	return Res;
}

void PSQL::SetDB(QString a_DB)
{
	Database.close();
	Database.setDatabaseName(a_DB);
	Database.open();
}

