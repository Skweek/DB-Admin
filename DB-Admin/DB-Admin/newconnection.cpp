#include "newconnection.hpp"

NewConnection::NewConnection(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

	connect(ui.btnConnect, SIGNAL(clicked()), this, SLOT(Connect()));
}

NewConnection::~NewConnection()
{
}

void NewConnection::Connect()
{
	Host = ui.txtHost->text();
	User = ui.txtUser->text();
	Pass = ui.txtPassword->text();
	DB = ui.txtDB->text();
	Port = ui.numPort->value();

	done(DialogCode::Accepted);
}