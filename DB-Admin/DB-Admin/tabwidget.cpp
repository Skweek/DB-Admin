#include "tabwidget.h"
#include <QEvent>
#include <qdebug.h>
#include <QTabBar>
#include <QMouseEvent>

TabWidget::TabWidget(QWidget *parent) : QTabWidget(parent)
{
	installEventFilter(this);
}

TabWidget::~TabWidget()
{
}

bool TabWidget::eventFilter(QObject* o, QEvent* e)
{
	if (e->type() == QEvent::MouseButtonPress) {
		auto mouseEvent = static_cast<QMouseEvent*>(e);
		if (mouseEvent->button() == Qt::MouseButton::MiddleButton)
		{
			auto tab = tabBar()->tabAt(mouseEvent->pos());
			if(tab > -1) removeTab(tab);
		}
		return true;
	}

	return QTabWidget::eventFilter(o, e);
}
