/********************************************************************************
** Form generated from reading UI file 'selectwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTWINDOW_H
#define UI_SELECTWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SelectWindow
{
public:
    QGridLayout *gridLayout;
    QFrame *line;
    QTableView *tblResults;
    QToolButton *btnRun;
    QTextEdit *txtFilter;

    void setupUi(QWidget *SelectWindow)
    {
        if (SelectWindow->objectName().isEmpty())
            SelectWindow->setObjectName(QStringLiteral("SelectWindow"));
        SelectWindow->resize(1376, 672);
        gridLayout = new QGridLayout(SelectWindow);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        line = new QFrame(SelectWindow);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 2, 0, 1, 2);

        tblResults = new QTableView(SelectWindow);
        tblResults->setObjectName(QStringLiteral("tblResults"));
        tblResults->setAlternatingRowColors(true);
        tblResults->setSortingEnabled(true);

        gridLayout->addWidget(tblResults, 3, 0, 1, 2);

        btnRun = new QToolButton(SelectWindow);
        btnRun->setObjectName(QStringLiteral("btnRun"));

        gridLayout->addWidget(btnRun, 0, 0, 1, 1);

        txtFilter = new QTextEdit(SelectWindow);
        txtFilter->setObjectName(QStringLiteral("txtFilter"));
        txtFilter->setEnabled(true);
        txtFilter->setMaximumSize(QSize(16777215, 100));
        txtFilter->setReadOnly(false);

        gridLayout->addWidget(txtFilter, 1, 0, 1, 2);


        retranslateUi(SelectWindow);

        QMetaObject::connectSlotsByName(SelectWindow);
    } // setupUi

    void retranslateUi(QWidget *SelectWindow)
    {
        SelectWindow->setWindowTitle(QApplication::translate("SelectWindow", "SelectWindow", Q_NULLPTR));
        btnRun->setText(QApplication::translate("SelectWindow", "Run Query", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SelectWindow: public Ui_SelectWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTWINDOW_H
