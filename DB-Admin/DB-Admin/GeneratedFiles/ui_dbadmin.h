/********************************************************************************
** Form generated from reading UI file 'dbadmin.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DBADMIN_H
#define UI_DBADMIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include <tabwidget.h>

QT_BEGIN_NAMESPACE

class Ui_DBAdminClass
{
public:
    QAction *mnuNewConnection;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    TabWidget *tabConnections;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menu;

    void setupUi(QMainWindow *DBAdminClass)
    {
        if (DBAdminClass->objectName().isEmpty())
            DBAdminClass->setObjectName(QStringLiteral("DBAdminClass"));
        DBAdminClass->resize(1023, 584);
        mnuNewConnection = new QAction(DBAdminClass);
        mnuNewConnection->setObjectName(QStringLiteral("mnuNewConnection"));
        centralWidget = new QWidget(DBAdminClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tabConnections = new TabWidget(centralWidget);
        tabConnections->setObjectName(QStringLiteral("tabConnections"));

        gridLayout->addWidget(tabConnections, 0, 0, 1, 1);

        DBAdminClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(DBAdminClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DBAdminClass->setStatusBar(statusBar);
        menuBar = new QMenuBar(DBAdminClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1023, 21));
        menu = new QMenu(menuBar);
        menu->setObjectName(QStringLiteral("menu"));
        DBAdminClass->setMenuBar(menuBar);

        menuBar->addAction(menu->menuAction());
        menu->addAction(mnuNewConnection);

        retranslateUi(DBAdminClass);

        tabConnections->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(DBAdminClass);
    } // setupUi

    void retranslateUi(QMainWindow *DBAdminClass)
    {
        DBAdminClass->setWindowTitle(QApplication::translate("DBAdminClass", "DBAdmin", Q_NULLPTR));
        mnuNewConnection->setText(QApplication::translate("DBAdminClass", "New Connection", Q_NULLPTR));
        menu->setTitle(QApplication::translate("DBAdminClass", "Connections", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DBAdminClass: public Ui_DBAdminClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DBADMIN_H