/********************************************************************************
** Form generated from reading UI file 'querywindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QUERYWINDOW_H
#define UI_QUERYWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QueryWindow
{
public:
    QGridLayout *gridLayout;
    QToolButton *btnRun;
    QTextEdit *txtQuery;
    QFrame *line;
    QTableWidget *tblResults;

    void setupUi(QWidget *QueryWindow)
    {
        if (QueryWindow->objectName().isEmpty())
            QueryWindow->setObjectName(QStringLiteral("QueryWindow"));
        QueryWindow->resize(859, 499);
        gridLayout = new QGridLayout(QueryWindow);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        btnRun = new QToolButton(QueryWindow);
        btnRun->setObjectName(QStringLiteral("btnRun"));

        gridLayout->addWidget(btnRun, 0, 0, 1, 1);

        txtQuery = new QTextEdit(QueryWindow);
        txtQuery->setObjectName(QStringLiteral("txtQuery"));
        txtQuery->setMaximumSize(QSize(16777215, 300));

        gridLayout->addWidget(txtQuery, 1, 0, 1, 2);

        line = new QFrame(QueryWindow);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 2, 0, 1, 2);

        tblResults = new QTableWidget(QueryWindow);
        tblResults->setObjectName(QStringLiteral("tblResults"));
        tblResults->setAlternatingRowColors(true);
        tblResults->setSortingEnabled(true);

        gridLayout->addWidget(tblResults, 3, 0, 1, 2);


        retranslateUi(QueryWindow);

        QMetaObject::connectSlotsByName(QueryWindow);
    } // setupUi

    void retranslateUi(QWidget *QueryWindow)
    {
        QueryWindow->setWindowTitle(QApplication::translate("QueryWindow", "QueryWindow", Q_NULLPTR));
        btnRun->setText(QApplication::translate("QueryWindow", "Run Query", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class QueryWindow: public Ui_QueryWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QUERYWINDOW_H