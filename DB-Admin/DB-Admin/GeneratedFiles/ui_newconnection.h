/********************************************************************************
** Form generated from reading UI file 'newconnection.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWCONNECTION_H
#define UI_NEWCONNECTION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NewConnection
{
public:
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *hostLabel;
    QLineEdit *txtHost;
    QLabel *usernameLabel;
    QLineEdit *txtUser;
    QLabel *passwordLabel;
    QLineEdit *txtPassword;
    QLabel *portLabel;
    QSpinBox *numPort;
    QLabel *defaultDBLabel;
    QLineEdit *txtDB;
    QCommandLinkButton *btnConnect;

    void setupUi(QWidget *NewConnection)
    {
        if (NewConnection->objectName().isEmpty())
            NewConnection->setObjectName(QStringLiteral("NewConnection"));
        NewConnection->resize(242, 196);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(NewConnection->sizePolicy().hasHeightForWidth());
        NewConnection->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(NewConnection);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        hostLabel = new QLabel(NewConnection);
        hostLabel->setObjectName(QStringLiteral("hostLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, hostLabel);

        txtHost = new QLineEdit(NewConnection);
        txtHost->setObjectName(QStringLiteral("txtHost"));

        formLayout->setWidget(0, QFormLayout::FieldRole, txtHost);

        usernameLabel = new QLabel(NewConnection);
        usernameLabel->setObjectName(QStringLiteral("usernameLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, usernameLabel);

        txtUser = new QLineEdit(NewConnection);
        txtUser->setObjectName(QStringLiteral("txtUser"));

        formLayout->setWidget(2, QFormLayout::FieldRole, txtUser);

        passwordLabel = new QLabel(NewConnection);
        passwordLabel->setObjectName(QStringLiteral("passwordLabel"));

        formLayout->setWidget(3, QFormLayout::LabelRole, passwordLabel);

        txtPassword = new QLineEdit(NewConnection);
        txtPassword->setObjectName(QStringLiteral("txtPassword"));

        formLayout->setWidget(3, QFormLayout::FieldRole, txtPassword);

        portLabel = new QLabel(NewConnection);
        portLabel->setObjectName(QStringLiteral("portLabel"));

        formLayout->setWidget(1, QFormLayout::LabelRole, portLabel);

        numPort = new QSpinBox(NewConnection);
        numPort->setObjectName(QStringLiteral("numPort"));
        numPort->setMinimum(1);
        numPort->setMaximum(25565);
        numPort->setValue(5432);

        formLayout->setWidget(1, QFormLayout::FieldRole, numPort);

        defaultDBLabel = new QLabel(NewConnection);
        defaultDBLabel->setObjectName(QStringLiteral("defaultDBLabel"));

        formLayout->setWidget(4, QFormLayout::LabelRole, defaultDBLabel);

        txtDB = new QLineEdit(NewConnection);
        txtDB->setObjectName(QStringLiteral("txtDB"));

        formLayout->setWidget(4, QFormLayout::FieldRole, txtDB);


        gridLayout->addLayout(formLayout, 0, 0, 1, 1);

        btnConnect = new QCommandLinkButton(NewConnection);
        btnConnect->setObjectName(QStringLiteral("btnConnect"));

        gridLayout->addWidget(btnConnect, 1, 0, 1, 1);


        retranslateUi(NewConnection);

        QMetaObject::connectSlotsByName(NewConnection);
    } // setupUi

    void retranslateUi(QWidget *NewConnection)
    {
        NewConnection->setWindowTitle(QApplication::translate("NewConnection", "NewConnection", Q_NULLPTR));
        hostLabel->setText(QApplication::translate("NewConnection", "Host", Q_NULLPTR));
        usernameLabel->setText(QApplication::translate("NewConnection", "Username", Q_NULLPTR));
        txtUser->setText(QApplication::translate("NewConnection", "postgres", Q_NULLPTR));
        passwordLabel->setText(QApplication::translate("NewConnection", "Password", Q_NULLPTR));
        portLabel->setText(QApplication::translate("NewConnection", "Port", Q_NULLPTR));
        defaultDBLabel->setText(QApplication::translate("NewConnection", "Default DB", Q_NULLPTR));
        txtDB->setText(QApplication::translate("NewConnection", "postgres", Q_NULLPTR));
        btnConnect->setText(QApplication::translate("NewConnection", "Connect", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class NewConnection: public Ui_NewConnection {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWCONNECTION_H
