/********************************************************************************
** Form generated from reading UI file 'connectioncontainer.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONNECTIONCONTAINER_H
#define UI_CONNECTIONCONTAINER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QWidget>
#include <tabwidget.h>

QT_BEGIN_NAMESPACE

class Ui_ConnectionContainer
{
public:
    QGridLayout *gridLayout_2;
    QFrame *frame;
    QGridLayout *gridLayout;
    QComboBox *cmbDatabases;
    QListWidget *listWidget;
    QComboBox *cmbObjects;
    QCommandLinkButton *btnNewQuery;
    TabWidget *connectionTabs;

    void setupUi(QWidget *ConnectionContainer)
    {
        if (ConnectionContainer->objectName().isEmpty())
            ConnectionContainer->setObjectName(QStringLiteral("ConnectionContainer"));
        ConnectionContainer->resize(1177, 687);
        gridLayout_2 = new QGridLayout(ConnectionContainer);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        frame = new QFrame(ConnectionContainer);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setMaximumSize(QSize(250, 16777215));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        cmbDatabases = new QComboBox(frame);
        cmbDatabases->setObjectName(QStringLiteral("cmbDatabases"));
        cmbDatabases->setMaximumSize(QSize(250, 16777215));

        gridLayout->addWidget(cmbDatabases, 0, 0, 1, 1);

        listWidget = new QListWidget(frame);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setMaximumSize(QSize(250, 16777215));
        listWidget->setContextMenuPolicy(Qt::CustomContextMenu);
        listWidget->setAlternatingRowColors(true);

        gridLayout->addWidget(listWidget, 2, 0, 1, 1);

        cmbObjects = new QComboBox(frame);
        cmbObjects->setObjectName(QStringLiteral("cmbObjects"));
        cmbObjects->setMaximumSize(QSize(250, 16777215));

        gridLayout->addWidget(cmbObjects, 1, 0, 1, 1);

        btnNewQuery = new QCommandLinkButton(frame);
        btnNewQuery->setObjectName(QStringLiteral("btnNewQuery"));

        gridLayout->addWidget(btnNewQuery, 3, 0, 1, 1);


        gridLayout_2->addWidget(frame, 0, 0, 1, 1);

        connectionTabs = new TabWidget(ConnectionContainer);
        connectionTabs->setObjectName(QStringLiteral("connectionTabs"));

        gridLayout_2->addWidget(connectionTabs, 0, 1, 1, 1);


        retranslateUi(ConnectionContainer);

        QMetaObject::connectSlotsByName(ConnectionContainer);
    } // setupUi

    void retranslateUi(QWidget *ConnectionContainer)
    {
        ConnectionContainer->setWindowTitle(QApplication::translate("ConnectionContainer", "ConnectionContainer", Q_NULLPTR));
        cmbObjects->clear();
        cmbObjects->insertItems(0, QStringList()
         << QString()
         << QApplication::translate("ConnectionContainer", "Tables", Q_NULLPTR)
         << QApplication::translate("ConnectionContainer", "Functions", Q_NULLPTR)
         << QApplication::translate("ConnectionContainer", "Sequences", Q_NULLPTR)
         << QApplication::translate("ConnectionContainer", "Views", Q_NULLPTR)
         << QApplication::translate("ConnectionContainer", "Types", Q_NULLPTR)
        );
        btnNewQuery->setText(QApplication::translate("ConnectionContainer", "New Query", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ConnectionContainer: public Ui_ConnectionContainer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONNECTIONCONTAINER_H