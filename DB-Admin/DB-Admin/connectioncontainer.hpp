#pragma once

#include <QWidget>
#include "ui_connectioncontainer.h"

class SqlConnector;

class ConnectionContainer : public QWidget
{
	Q_OBJECT

public:
	ConnectionContainer(SqlConnector* a_Connection, QWidget *parent = Q_NULLPTR);
	~ConnectionContainer();

	SqlConnector* GetConnection() { return Connection; }

private:
	Ui::ConnectionContainer ui;
	SqlConnector* Connection;

	void NewTab(QString a_Query);
	void NewTab(QString a_Table, QString a_Filter);

private slots:
	void NewQueryTab();
	void DatabaseChanged();
	void ObjectTypeChanged();
	void BuildContextMenu(QPoint a_Location);


};