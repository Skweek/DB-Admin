#include "connectioncontainer.hpp"
#include "querywindow.hpp"
#include "selectwindow.hpp"
#include "psql.hpp"

#include <QMenu>

ConnectionContainer::ConnectionContainer(SqlConnector* a_Connection, QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);

	Connection =  a_Connection;

	// Get list of databases
	auto Res = Connection->Query("SELECT datname FROM pg_database WHERE datistemplate IS FALSE;");
	for each (auto var in Res)
	{
		if (var.find("datname").value().canConvert<QString>())
		{
			ui.cmbDatabases->addItem(var.find("datname").value().toString());
		}
	}

	connect(ui.btnNewQuery, SIGNAL(clicked()), this, SLOT(NewQueryTab())); 
	connect(ui.cmbDatabases, SIGNAL(currentIndexChanged(int)), this, SLOT(DatabaseChanged()));
	connect(ui.cmbObjects, SIGNAL(currentIndexChanged(int)), this, SLOT(ObjectTypeChanged()));
	connect(ui.listWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(BuildContextMenu(QPoint)));
}

ConnectionContainer::~ConnectionContainer()
{
}

void ConnectionContainer::NewTab(QString a_Query)
{
	ui.connectionTabs->addTab(new QueryWindow(GetConnection(), ui.cmbDatabases->currentText(), a_Query), "Query");
	ui.connectionTabs->setCurrentIndex(ui.connectionTabs->count());
}

void ConnectionContainer::NewTab(QString a_Table, QString a_Filter)
{
	ui.connectionTabs->addTab(new SelectWindow(GetConnection(), a_Table, a_Filter, ""), "Query");
	ui.connectionTabs->setCurrentIndex(ui.connectionTabs->count());
}

void ConnectionContainer::DatabaseChanged()
{
	Connection->SetDB(ui.cmbDatabases->currentText());

	ObjectTypeChanged();
}

void ConnectionContainer::ObjectTypeChanged()
{
	// Clear object list
	ui.listWidget->clear();

	QString Query = "";

	if (ui.cmbObjects->currentText() == "Tables")
	{
		Query = Connection->GET_TABLES;
	}
	else if (ui.cmbObjects->currentText() == "Views")
	{
		Query = Connection->GET_VIEWS;
	}
	else if (ui.cmbObjects->currentText() == "Functions")
	{
		Query = Connection->GET_FUNCTIONS;
	}
	else if (ui.cmbObjects->currentText() == "Sequences")
	{
		Query = Connection->GET_SEQUENCES;
	}
	else if (ui.cmbObjects->currentText() == "Types")
	{
		Query = Connection->GET_TYPES;
	}

	if (!Query.isEmpty())
	{
		auto Res = Connection->Query(Query);
		for each (auto var in Res)
		{
			if (var.find("name").value().canConvert<QString>())
			{
				ui.listWidget->addItem(var.find("name").value().toString());
			}
		}
	}
}

void ConnectionContainer::BuildContextMenu(QPoint a_Location)
{
	// Only show menu if we have clicked an actual item
	if (ui.listWidget->selectedItems().count() == 0) return;

	QMenu menu(ui.listWidget);

	// Add actions
	QMenu* ViewData = menu.addMenu("View Data");
	QAction* ViewDataAll = ViewData->addAction("View All Rows");
	QAction* ViewData1000 = ViewData->addAction("View Filtered Rows");
	QAction* QueryWindow = ViewData->addAction("New SQL Query");

	// Handle selection
	auto selected = menu.exec(ui.listWidget->mapToGlobal(a_Location));
	if (selected == ViewData1000)
	{
		NewTab(ui.listWidget->currentItem()->text(), "<insert filter here>");
	}
	if (selected == ViewDataAll)
	{
		NewQueryTab();
	}
}

void ConnectionContainer::NewQueryTab()
{
	NewTab("");
}
