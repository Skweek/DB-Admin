#pragma once

#include <QWidget>
#include "ui_querywindow.h"

class QueryWindow : public QWidget
{
	Q_OBJECT

public:
	QueryWindow(class SqlConnector* a_Connection, QString a_DB, QString a_Query = "", QWidget *parent = Q_NULLPTR);
	~QueryWindow();

private:
	Ui::QueryWindow ui;
	class SqlConnector* Connection;

private slots:
	void RunQuery();
};