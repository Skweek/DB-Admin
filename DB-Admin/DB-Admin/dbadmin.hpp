#ifndef DBADMIN_HPP
#define DBADMIN_HPP

#include <QtWidgets/QMainWindow>
#include "ui_dbadmin.h"

class DBAdmin : public QMainWindow
{
	Q_OBJECT

public:
	DBAdmin(QWidget *parent = 0);
	~DBAdmin();

private:
	Ui::DBAdminClass ui;

private slots:
	void AddConnection();
};

#endif // DBADMIN_HPP
