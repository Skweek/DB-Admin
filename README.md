Public Trello Roadmap: https://trello.com/b/oW3YzfVW/db-admin 

DB-Admin is written using Qt5 open source. The full source code can be downloaded at https://www.qt.io.
All source code falls under LGVL V3 (https://www.gnu.org/licenses/lgpl-3.0.en.html).

No current plans are in place for feature requests/pull requests. This program is available with no warranty or guranteed support.
All requests and comments can be sent to dbadmin at ncsmith.com.au